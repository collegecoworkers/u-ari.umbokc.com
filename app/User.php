<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [ 'name', 'email', 'password' ];

	protected $hidden = [ 'password', 'remember_token' ];

	static function id() { return self::curr()->id; }
	function thisIsAdmin() { return $this->role == 'admin'; }
	static function isAdmin() { return self::curr()->role == 'admin'; }
	static function isUser() { return !self::isAdmin(); }
	static function curr() { return auth()->user(); }

	function getRole() { return self::getRoleOf($this->role);}
	static function getRoles(){ return [ 'user' => 'Пользователь', 'admin' => 'Админ',]; }
	static function getRoleOf($r){ $def = 'user'; $items = self::getRoles(); if(array_key_exists($r, $items)) return $items[$r]; return $items[$def]; }

	static function allArr($field = 'name'){ return F::toArr(self::all(), $field, 'id'); }

	static function getById($val){ return self::getBy('id', $val); }
	static function getBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->first(); else return self::queryBy([$col => $val])->first(); }
	static function getsBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->get(); else return self::queryBy([$col => $val])->get(); }
	static function queryBy($arr){ return self::where($arr); }
}
