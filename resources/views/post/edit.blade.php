@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="{{ url()->current() }}">Изменить тему</a></li>
	</ol>
</div>

<div class="row">
	<div class="col-lg-8 col-lg-offset-2">
		<h2>Изменить тему</h2>
		<br>
		<br>
		@include('post._form')
	</div>
</div>
@endsection
