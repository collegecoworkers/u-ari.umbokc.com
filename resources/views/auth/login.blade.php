@extends('layout.app')
@section('content')

<div class="row">
	<div class="col-lg-6 col-lg-offset-3">
		<h2>Вход</h2>
		<br>
		<br>
		<form class="form-login" method="post" action="{{ route('login') }}">
			{{ csrf_field() }}
			<div class="form-group">
				<input class="form-control" name="email" placeholder="Email" type="text" />
				@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
			</div>
			<div class="form-group">
				<input class="form-control" name="password" placeholder="Пароль" type="password" />
				@if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif
			</div>
			<button type="submit" class="btn btn-primary">Войти</button>
			<a class="" href="/register">Создать аккаунт</a>
		</form>
	</div>
</div>
@endsection
