{!! Form::open(['url' => isset($model) ? '/cat/update/'.$model->id : '/cat/create/' ]) !!}

	<div class="form-group">
		<label class="control-label">Название</label>
		<input name="title" value="{{ isset($model) ? $model->title : ''}}" type="text" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">Описание</label>
		<textarea name="desc" class="form-control" required>{{ isset($model) ? $model->desc : ''}}</textarea>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Отправить</button>
	</div>
{!! Form::close() !!}

