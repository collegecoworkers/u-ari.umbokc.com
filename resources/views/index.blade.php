@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
	</ol>
</div>

<h2>Главная</h2>

<table class="table table-index">
	<thead>
		<tr>
			<th>Категория</th>
			<th class="col-md-2">Тем</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($cats as $item)
			<tr>
				<td><p class="lead"><a href="/posts/{{ $item->id }}">{{ $item->title }}</a></p></td>
				<td>{{ $item->countPost() }}</td>
			</tr>
		@endforeach
	</tbody>
</table>

@endsection
