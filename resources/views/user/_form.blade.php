{!! Form::open(['url' => isset($model) ? '/user/update/'.$model->id : '/user/create/' ]) !!}

	<div class="form-group">
		<label>Логин:</label>
    {!! Form::text('name', isset($model) ? $model->name : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Email:</label>
    {!! Form::text('email', isset($model) ? $model->email : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Роль:</label>
		{!! Form::select('role', $roles, isset($model) ? $model->role : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Пароль:</label>
    {!! Form::password('password', ['class' => 'form-control']) !!}
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Отправить</button>
	</div>
{!! Form::close() !!}

