<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Post,
	User
};

class AdminController extends Controller {

	function __construct(){
	}

	function Send() {
		$posts = Post::where('private', true)->orderBy('id', 'desc')->get();
		return view('send.all')->with([
			'posts' => $posts,
		]);
	}
	function Edit($id) {
		$cat = Cat::getById($id);
		return view('cat.edit')->with([
			'model' => $cat,
		]);
	}
	function Create(Request $request) {
		$model = new Cat();

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/admin/cats');
	}
	function Update($id, Request $request) {
		$model = Cat::getById($id);

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/admin/cats');
	}
	function Delete($id) {
		Cat::where('id', $id)->delete();
		return redirect()->back();
	}
}
