<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cats extends Migration
{

	public function up()
	{
		Schema::create('cats', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->text('desc');

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	public function down()
	{
		//
	}
}
