<?php

Auth::routes();

Route::get('/', 'SiteController@Index');
Route::get('/account', 'SiteController@Account');
Route::get('/send', 'SiteController@Send');
Route::post('/sendsave', 'SiteController@SendSave');

Route::get('/admin/cats', 'CatController@All');
Route::get('/cat/add', 'CatController@Add');
Route::get('/cat/edit/{id}', 'CatController@Edit');
Route::get('/cat/delete/{id}', 'CatController@Delete');
Route::post('/cat/create', 'CatController@Create');
Route::post('/cat/update/{id}', 'CatController@Update');

Route::get('/posts/{cat_id}', 'PostController@All');
Route::get('/post/view/{id}', 'PostController@View');
Route::get('/post/add/{cat_id}', 'PostController@Add');
Route::get('/post/edit/{id}', 'PostController@Edit');
Route::get('/post/delete/{id}', 'PostController@Delete');
Route::post('/post/create/{cat_id}', 'PostController@Create');
Route::post('/post/update/{id}', 'PostController@Update');
Route::post('/post/post/{id}', 'PostController@Post');

Route::get('/admin/send', 'AdminController@Send');

Route::get('/admin/users', 'UserController@Index');
Route::get('/user/add', 'UserController@Add');
Route::get('/user/edit/{id}', 'UserController@Edit');
Route::get('/user/delete/{id}', 'UserController@Delete');
Route::post('/user/create', 'UserController@Create');
Route::post('/user/update/{id}', 'UserController@Update');
