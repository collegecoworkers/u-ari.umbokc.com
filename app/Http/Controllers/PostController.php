<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Cat,
	Post,
	Message,
	User
};

class PostController extends Controller {

	function __construct(){
	}

	function View($id) {
		$item = Post::getBy('id', $id);
		return view('post.view')->with([
			'cat' => Cat::getById($item->cat_id),
			'item' => $item,
			'messages' => Message::getsBy('post_id', $item->id),
		]);
	}
	function All($cat_id) {
		$cat = Cat::getById($cat_id);
		$items = Post::where(['cat_id' => $cat->id, 'private' => false])->orderBy('id', 'desc')->get();

		return view('post.all')->with([
			'items' => $items,
			'cat' => $cat,
		]);
	}
	function Add($cat_id) {
		return view('post.add')->with([
			'cat_id' => $cat_id,
		]);
	}
	function Edit($id) {
		$post = Post::getById($id);
		return view('post.edit')->with([
			'model' => $post,
		]);
	}
	function Create($cat_id, Request $request) {
		$model = new Post();

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->cat_id = $cat_id;
		$model->user_id = User::id();

		$model->save();
		return redirect('/posts/' . $cat_id);
	}
	function Update($id, Request $request) {
		$model = Post::getById($id);

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/posts/' . $model->cat_id);
	}
	function Post($id, Request $request) {
		$model = new Message();

		$model->post_id = $id;
		$model->content = request()->content;
		$model->user_id = User::id();

		$model->save();
		return redirect()->back();
	}
	function Delete($id) {
		Post::where('id', $id)->delete();
		return redirect()->back();
	}
}
