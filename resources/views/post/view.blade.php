@php
	use App\User;
@endphp
@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		@if (!isset($cat))
			<li><a href="/account">Приватные темы</a></li>
		@else
			<li><a href="/posts/{{ $cat->id }}">{{ $cat->title }}</a></li>
		@endif
		<li><a href="/post/view/{{ $item->id }}">{{ $item->title }}</a></li>
	</ol>
</div>

<h2>{{ $item->title }}</h2>

<table class="table ">
	<thead>
		<tr>
			<th class="col-md-2">Автор</th>
			<th>Сообщение</th>
			<th class="col-md-2">Дата отправки</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($messages as $i)
			@auth
				<tr class="post-body {{ User::id() == $i->user_id ? 'selected' : '' }} {{ User::getById($i->user_id)->thisIsAdmin() && $item->private ? 'selected-admin' : '' }}">
			@endauth
			@guest
				<tr class="post-body">
			@endguest
				<td class="author-info"><strong>{{ $i->getUser()->name }}</strong></td>
				<td class="content">
					{{ $i->content }}
				</td>
				<td class="content">{{ $i->getDate() }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
@auth
	<h3>Ответить</h3>
	<div id="quick-reply">
		<form method="POST" action="/post/post/{{ $item->id }}">
			{{ csrf_field() }}
			<div class="form-group">
				<textarea name="content" class="form-control"></textarea>
			</div>

			<div class="text-right">
				<button type="submit" class="btn btn-success pull-right">Отправить</button>
			</div>
		</form>
	</div>
@endauth
<hr>

@endsection
