@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="/posts/{{ $cat->id }}">{{ $cat->title }}</a></li>
	</ol>
</div>

<div class="row">
	<h2>{{ $cat->title }}</h2>
	@auth
	<div class="">
		<a href="/post/add/{{ $cat->id }}" class="btn btn-primary">Создать тему</a>
	</div>
	@endauth

	@include('post._table', ['the_items_' => $items, 'big_header' => 1])
	
	@auth
	<div class="">
		<a href="/post/add/{{ $cat->id }}" class="btn btn-primary">Создать тему</a>
	</div>
	@endauth

</div>
@endsection
