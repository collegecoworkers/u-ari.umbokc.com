@php
	use App\User;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name', 'Laravel') }}</title>

	<link href="/css/app.css" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>textarea { min-height: 200px; } .deleted { opacity: 0.65; }</style>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/">Главная</a></li>
					@auth
						@if (!User::isAdmin())
							<li><a href="/send" >Написать техподдержке</a></li>
						@endif
					@endauth
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@auth
						<li><a href="/account" >Личный кабинет</a></li>
						@if (User::isAdmin())
							<li><a href="/admin/users" >Пользователи</a></li>
							<li><a href="/admin/cats" >Категории</a></li>
							<li><a href="/admin/send" >Почта</a></li>
						@endif
						<li>
							<form id="logout-form" action="logout" method="POST" style="display: none;">{{ csrf_field() }}</form>
							<a class="logout" href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
						</li>
					@endauth
					@guest
						<li><a href="/login" >Вход</a></li>
					@endguest
				</ul>
			</div>
		</div>
	</nav>

	<div id="main" class="container">
		
		@yield('content')

		<hr>
		<div class="text-center">
			<p>&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</p>
		</div>
	</div>

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<script>
		$(document).ready(function() {
			$('ul.nav li a').each(function() {
				if ($(this).attr('href') === window.location.pathname)
				{
					$(this).parent('li').addClass('active');
				}
			});
		});

		var toggle = $('input[type=checkbox][data-toggle-all]');
		var checkboxes = $('table tbody input[type=checkbox]');
		var actions = $('[data-actions]');
		var forms = $('[data-actions-form]');
		var confirmString = "Are you sure?";

		function setToggleStates() {
			checkboxes.prop('checked', toggle.is(':checked')).change();
		}

		function setSelectionStates() {
			checkboxes.each(function() {
				var tr = $(this).parents('tr');

				$(this).is(':checked') ? tr.addClass('active') : tr.removeClass('active');

				checkboxes.filter(':checked').length ? $('[data-bulk-actions]').removeClass('hidden') : $('[data-bulk-actions]').addClass('hidden');
			});
		}

		function setActionStates() {
			forms.each(function () {
				var form = $(this);
				var method = form.find('input[name=_method]');
				var selected = form.find('select[name=action] option:selected');
				var depends = form.find('[data-depends]');

				selected.each(function () {
					if ($(this).attr('data-method')) {
						method.val($(this).data('method'));
					} else {
						method.val('patch');
					}
				});

				depends.each(function () {
					(selected.val() == $(this).data('depends')) ? $(this).removeClass('hidden') : $(this).addClass('hidden');
				});
			});
		}

		setToggleStates();
		setSelectionStates();
		setActionStates();

		toggle.click(setToggleStates);
		checkboxes.change(setSelectionStates);
		actions.change(setActionStates);

		forms.submit(function () {
			var action = $(this).find('[data-actions]').find(':selected');

			if (action.is('[data-confirm]')) {
				return confirm(confirmString);
			}

			return true;
		});

		$('form[data-confirm]').submit(function () {
			return confirm(confirmString);
		});
	</script>
</body>
</html>
