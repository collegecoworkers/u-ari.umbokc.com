@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="/account">Личный кабинет</a></li>
	</ol>
</div>

<h2>Личный кабинет</h2>

<h3>Мои темы</h3>
@include('post._table', ['the_items_' => $posts])

<h3>Мои обращения в техподдержку</h3>
@include('post._table', ['the_items_' => $sends])

@endsection
