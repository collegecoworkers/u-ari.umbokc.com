@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="{{ url()->current() }}">Список пользователей</a></li>
	</ol>
</div>

<div class="row">
	<h2>Список пользователей</h2>
	<table class="table table-index">
		<thead>
			<tr>
				<th>#</th>
				<th>Имя</th>
				<th>Логин</th>
				<th>Email</th>
				<th>Статус</th>
				<th>Действия</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $item)
			<tr>
				<td ta:c>{{$item->id}}</td>
				<td ta:c>{{$item->full_name}}</td>
				<td ta:c>{{$item->name}}</td>
				<td ta:c>{{$item->email}}</td>
				<td ta:c>{{$item->getRole()}}</td>
				<td>
					<a href="/user/edit/{{ $item->id  }}">
						<i class="fa fa-edit"></i>
					</a>
					<a href="/user/delete/{{ $item->id  }}" onclick="return confirm('Вы уверенны?')">
						<i class="fa fa-trash"></i>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
