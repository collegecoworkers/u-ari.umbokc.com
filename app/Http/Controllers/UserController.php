<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
  User
};

class UserController extends Controller
{

  public function __construct(){
    $this->middleware('auth');
  }
  public function Index() {
    $users = User::all();
    return view('user.all')->with([
      'users' => $users,
    ]);
  }
  public function Add() {
    return view('user.add')->with([
      'roles' => User::getRoles(),
    ]);
  }
  public function Edit($id) {
    $model = User::getBy('id', $id);
    return view('user.edit')->with([
      'roles' => User::getRoles(),
      'model' => $model,
    ]);
  }
  public function Delete($id) {
    User::where('id', $id)->delete();
    return redirect()->to('/admin/users');
  }
  public function Create(Request $request) {
    $m = new User();

    $m->name = request()->name;
    $m->role = request()->role;
    $m->email = request()->email;
    $m->password = bcrypt(request()->password);

    $m->save();
    return redirect('/admin/users');
  }
  public function Update($id, Request $request) {
    $m = User::getBy('id', $id);

    $m->name = request()->name;
    $m->role = request()->role;
    $m->email = request()->email;
    if(trim(request()->password) !== '') $m->password = bcrypt(request()->password);

    $m->save();
    return redirect()->to('/admin/users');
  }

}
