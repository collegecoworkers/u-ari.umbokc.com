<?php
namespace App;

class Message extends MyModel{

	function getDate() {
		return date('d.m H:i', strtotime($this->created_at));
	}

	function getUser() {
		return User::getById($this->user_id);
	}
}
