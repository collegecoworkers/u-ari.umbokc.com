<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Post,
	Message,
	User
};

class SiteController extends Controller
{

	function __construct(){
	}

	function Index() {
		$cats = Cat::all();
		return view('index')->with([
			'cats' => $cats,
		]);
	}

	function Account() {
		$posts = Post::getsBy(['user_id' => User::id(), 'private' => !true]);
		$sends = Post::getsBy(['user_id' => User::id(), 'private' => true]);
		return view('account')->with([
			'posts' => $posts,
			'sends' => $sends,
		]);
	}

	function Send() {
		return view('send');
	}


	function SendSave(Request $request) {
		$model = new Post();

		$model->title = request()->title;
		$model->desc = 'Приватная тема';

		$model->cat_id = 0;
		$model->private = true;
		$model->user_id = User::id();

		$model->save();
		$model->desc = 'Приватная тема #' . $model->id;
		$model->save();

		$nm = new Message;

		$nm->post_id = $model->id;
		$nm->user_id = $model->user_id;
		$nm->content = request()->content;
		$nm->save();

		return redirect('/post/view/' . $model->id);
	}

}
