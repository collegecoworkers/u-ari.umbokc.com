<?php
namespace App;

class Post extends MyModel{

  function getLock() { return self::getLockOf($this->locked);}
  static function getLocks(){ return [ 0 => 'Активна', 1 => 'Закрыта',]; }
  static function getLockOf($r){ $def = 0; $items = self::getLocks(); if(array_key_exists($r, $items))  return $items[$r]; return $items[$def]; }

  function getDate() {
    return date('d.m H:i', strtotime($this->created_at));
  }

  function getUser() {
    return User::getById($this->user_id);
  }

	function countMessages() {
		return Message::where(['post_id' => $this->id])->count();
	}

	function countMyMessages() {
		return Message::where(['post_id' => $this->id, 'user_id' => User::id()])->count();
	}
}
