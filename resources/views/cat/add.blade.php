@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="{{ url()->current() }}">Создать категорию</a></li>
	</ol>
</div>

<div class="row">
	<div class="col-lg-8 col-lg-offset-2">
		<h2>Создать категорию</h2>
		<br>
		<br>
		@include('cat._form')
	</div>
</div>

@endsection
