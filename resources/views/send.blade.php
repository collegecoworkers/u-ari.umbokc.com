@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="/send">Написать техподдержке</a></li>
	</ol>
</div>

<div class="row">
	<div class="col-lg-8 col-lg-offset-2">
		<h2>Написать техподдержке</h2>
		<br>
		<br>
		<form class="form-login" method="post" action="/sendsave">
			{{ csrf_field() }}
			<div class="form-group">
				<input class="form-control" name="title" placeholder="Заголовок письма" type="text" />
				@if ($errors->has('title')) <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span> @endif
			</div>
			<div class="form-group">
				<textarea class="form-control" name="content" placeholder="Сообщение"></textarea>
				@if ($errors->has('content')) <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span> @endif
			</div>
			<button type="submit" class="btn btn-primary">Отправить</button>
		</form>
	</div>
</div>

@endsection
