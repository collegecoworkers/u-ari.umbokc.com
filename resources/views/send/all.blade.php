@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="{{ url()->current() }}">Список писем</a></li>
	</ol>
</div>

<div class="row">
	<h2>Список писем</h2>
	<table class="table table-index">
		<thead>
			<tr>
				<th>Название</th>
				<th class="col-md-2">Пользователь</th>
				<th class="col-md-2">Дата</th>
				<th class="col-md-2">Статус</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($posts as $item)
				<tr>
					<td>
						<p><a href="/post/view/{{ $item->id }}">{{ $item->title }}</a></p>
					</td>
					<td>{{ $item->getUser()->name }}</td>
					<td>{{ $item->getDate() }}</td>
					<td>{{ $item->getLock() }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>

</div>

@endsection
