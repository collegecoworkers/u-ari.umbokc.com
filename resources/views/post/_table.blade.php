<table class="table table-index">
  <thead>
    <tr>
      <th>Название</th>
      <th class="col-md-2">Статус</th>
      @auth
        <th class="col-md-2">Ваших сообщений</th>
      @endauth
      <th class="col-md-2">Сообщений</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($the_items_ as $item)
      <tr>
        <td>
          @isset ($big_header)
            <p class="lead"><a href="/post/view/{{ $item->id }}">{{ $item->title }}</a></p>
            <span>{{ $item->desc }}</span>
          @else
            <p><a href="/post/view/{{ $item->id }}">{{ $item->title }}</a></p>
          @endisset
        </td>
        <td>{{ $item->getLock() }}</td>
        @auth
          <td>{{ $item->countMyMessages() }}</td>
        @endauth
        <td>{{ $item->countMessages() }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
