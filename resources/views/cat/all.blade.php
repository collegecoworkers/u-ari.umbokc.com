@extends('layout.app')
@section('content')

<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Главная</a></li>
		<li><a href="{{ url()->current() }}">Список категорий</a></li>
	</ol>
</div>

<div class="row">
	<h2>Список категорий</h2>
	<div class="col-xs-4">
		<a href="/cat/add" class="btn btn-primary">Создать категорию</a>
	</div>
	<table class="table table-index">
		<thead>
			<tr>
				<th>Название</th>
				<th>Описание</th>
				<th class="col-md-2">Действия</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($cats as $item)
			<tr>
				<td>{{ $item->title }}</td>
				<td>{{ $item->desc }}</td>
				<td>
					<a href="/cat/edit/{{$item->id}}"><i class="fa fa-edit"></i></a>
					<a href="/cat/delete/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
